#include <stdbool.h>
#include <stdint.h>

//put your definition here
//Q1
#define MINIMUM_CURRENT 0.1
#define CURRENT_REFERENCE 1.0
#define VOLTAGE_REFERENCE 10.0

bool enable_command;
float current_feedback;
float voltage_feedback;

enum ChargerState {IDLE, CONSTANT_CURRENT, CONSTANT_VOLTAGE};
enum ChargerState charger_state;

//Q2
//CAN struct example
typedef struct {
   uint8_t Data[8];
   uint16_t Length;
   uint32_t ID;
} CAN_msg_typedef;

CAN_msg_typedef Can_tx;
CAN_msg_typedef Can_rx;

void CAN_write(CAN_msg_typedef *msg);
bool CAN_read(CAN_msg_typedef *msg); //return true if there is received msg

uint32_t time_ms;
float voltage_feedback_high;
float voltage_feedback_low;
float current_feedback_high;
float current_feedback_low;

// Enumeration for network states
enum NetworkState {
  INITIALIZATION,
  PRE_OPERATIONAL,
  OPERATIONAL
};

enum NetworkState network_state = INITIALIZATION;

void Initialization(void){
//initialize your variables here
   enable_command = false;
   current_feedback = 0.0;
   voltage_feedback = 0.0;
   charger_state = IDLE;
   time_ms = 0;
}

void control_routine(void){
   //run the control algorithm here
   switch (charger_state) {
      case CONSTANT_CURRENT:
         // PI current control algorithm
         break;
      case CONSTANT_VOLTAGE:
         // PI voltage control algorithm
         break;
      default:
         break;
   }
   time_ms++; //assume INT frequency is 1kHz, for timing purpose
}

void main_state_machine(void){
//run the state transition here
   switch (charger_state) {
      // Q1.a
      case IDLE: 
         if (enable_command) {
            charger_state = CONSTANT_CURRENT;
         }
         break;
      // Q1.b
      case CONSTANT_CURRENT:
         control_routine();
         if (voltage_feedback >= VOLTAGE_REFERENCE) {
            charger_state = CONSTANT_VOLTAGE;
            
         }
         break;
      // Q1.c
      case CONSTANT_VOLTAGE:
         control_routine();
         if (current_feedback <= MINIMUM_CURRENT) {
            enable_command = false;
            charger_state = IDLE;
         }
         break;
      default:
         break;
   }
}

void CAN_write_handler(void){
//CAN tx

}

void CAN_read_handler(void){
//CAN rx

}

void network_management(void){
   //run the network management here
   switch (network_state) {
      // Q2.a
      case INITIALIZATION:
         // Code for Initialization state
         voltage_feedback_high = 12.0;
         voltage_feedback_low = 5.0;
         current_feedback_high = 1.0;
         current_feedback_low = 0.1;

         //send INIT state
         Can_tx.ID = 0x701;
         Can_tx.Length = 1;
         Can_tx.Data[0] = 0;
         CAN_write(&Can_tx);

         //go to next state
         network_state = PRE_OPERATIONAL;

         break;
      // Q2.b
      case PRE_OPERATIONAL:
         // Code for Pre-Operational state
         // send pre_operational state 
         Can_tx.ID = 0x701;
         Can_tx.Length = 1;
         Can_tx.Data[0] = 1;
         CAN_write(&Can_tx);

         // Check for enable_command message
         if (CAN_read(&Can_rx) && Can_rx.ID == 0x201) {
            // if receive stop charging command 
            if(Can_rx.Data[4] == 1){
               //go to Operational state
               network_state = OPERATIONAL;
            }
         }
         break;
      // Q2.c
      case OPERATIONAL:
         // Code for Operational state
         // send operatinal state message
         Can_tx.ID = 0x701;
         Can_tx.Length = 1;
         Can_tx.Data[0] = 2;
         CAN_write(&Can_tx);

         //send outgoing message to BMS to start charging
         Can_tx.ID = 0x181;
         Can_tx.Length = 4;
         // in Hex
         Can_tx.Data[0] = voltage_feedback_high;
         Can_tx.Data[1] = voltage_feedback_low;
         Can_tx.Data[2] = current_feedback_high;
         Can_tx.Data[3] = current_feedback_low;
         Can_tx.Data[4] = 1;
         CAN_write(&Can_tx);

         // Check for stop charging command message
         if(CAN_read(&Can_rx) && Can_rx.ID == 0x201){
            if(Can_rx.Data[4] == 0){
               // Transition to Pre-Operational state
               network_state = PRE_OPERATIONAL;
            }
         }
         else{
            // if no message from bms for 5sec
            if (time_ms > 5000){
               //send outgoing message to BMS to stop charging
               Can_tx.ID = 0x181;
               Can_tx.Length = 4;
               Can_tx.Data[4] = 0;
               CAN_write(&Can_tx);

               // Transition to Pre-Operational state
               network_state = PRE_OPERATIONAL;
            }
         }
         break;
      default:
         break;
  }
}

void main(void){
   Initialization();
   PieVectTable.EPWM1_INT = &control_routine;
   while(true){
      main_state_machine();
      network_management();
}

// Q2.b
// Heartbeat during idle =     [0, 0, 0, 0, 0, 0, 0, 0]
// Heartbeat during charging = [0, 0, 0, 0, 0, 0, 0, 1]
// Heartbeat when stop charging = [0, 0, 0, 0, 0, 0, 0, 0]
// Incoming during start charging = [0, 0, 0, 1, 64, 0, 41, 01]
// Outgoing during start charging = [0, 0, 0, 1, 64, 0, 41, 01]
// Incoming during stop charging = [0, 0, 0, 0, 0, 0, 0, 0]
// Outgoing during stop charging = [0, 0, 0, 0, 0, 0, 0, 0]